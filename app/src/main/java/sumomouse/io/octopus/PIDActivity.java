package sumomouse.io.octopus;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PIDActivity extends AppCompatActivity {

    public static final String TAG = "PIDActivity";

    Handler h;

    @Bind(R.id.etKp)
    EditText etKp;

    @Bind(R.id.etKi)
    EditText etKi;

    @Bind(R.id.etKd)
    EditText etKd;


    @Bind(R.id.bt_send)
    Button sendButton;

    private final int RECEIVE_MESSAGE = 1;        // Status  for Handler

    private ConnectedThread mConnectedThread = ConnectedThread.mConnectedThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pid);

        ButterKnife.bind(this);

        h = new Handler(new Handler.Callback() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECEIVE_MESSAGE:                                                   // if receive massage
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);                 // create string from bytes array
                        StringBuilder sb = new StringBuilder();
                        sb.append(strIncom);                                                // append string
                        int endOfLineIndex = sb.indexOf("\r\n");                            // determine the end-of-line
                        if (endOfLineIndex > 0) {                                            // if end-of-line,
                            String sbPrint = sb.substring(0, endOfLineIndex);               // extract string
                            sb.delete(0, sb.length());                                      // and clear
                            Log.i(TAG,"Received: " + sbPrint);
                        }
                        break;
                }
                return true;
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = BluetoothCommands.START_CHAR + BluetoothCommands.PID;

                float p = Float.valueOf(etKp.getText().toString());
                byte[] pBytes = toByte(Float.floatToIntBits(p));

                float i = Float.valueOf(etKi.getText().toString());
                byte[] iBytes = toByte(Float.floatToIntBits(i));

                float d = Float.valueOf(etKd.getText().toString());
                byte[] dBytes = toByte(Float.floatToIntBits(d));

                if(mConnectedThread != null) {
                    mConnectedThread.write(msg);

                    mConnectedThread.writeByte(pBytes[0]);
                    mConnectedThread.writeByte(pBytes[1]);
                    mConnectedThread.writeByte(pBytes[2]);
                    mConnectedThread.writeByte(pBytes[3]);

                    mConnectedThread.writeByte(iBytes[0]);
                    mConnectedThread.writeByte(iBytes[1]);
                    mConnectedThread.writeByte(iBytes[2]);
                    mConnectedThread.writeByte(iBytes[3]);

                    mConnectedThread.writeByte(dBytes[0]);
                    mConnectedThread.writeByte(dBytes[1]);
                    mConnectedThread.writeByte(dBytes[2]);
                    mConnectedThread.writeByte(dBytes[3]);


                    mConnectedThread.write("\r");

                }

            }
        });



        mConnectedThread.setCallback(new ConnectedThread.BTCallback() {

            @Override
            public void onMessageReceived(int bytes, byte[] buffer) {
                Log.i(TAG,"Message Received");
                h.obtainMessage(RECEIVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Send to message queue Handler
            }
        });
    }

    byte[] toByte(int i)
    {
        byte[] result = new byte[4];

        result[3] = (byte) (i >> 24);
        result[2] = (byte) (i >> 16);
        result[1] = (byte) (i >> 8);
        result[0] = (byte) (i);

        return result;
    }
}
