package sumomouse.io.octopus;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectedThread extends Thread {
    private static final String TAG = "ConnectedThread";
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private BluetoothSocket btSocket;

    public static ConnectedThread mConnectedThread;

    private BTCallback callback;

    public ConnectedThread(BluetoothSocket socket,BTCallback callback) {
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        this.callback = callback;

        btSocket = socket;
        // Get the input and output streams, using temp objects because
        // member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.i(TAG,e.getMessage());
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
        mConnectedThread = this;
    }

    public void setCallback(BTCallback callback) {
        this.callback = callback;
    }

    public void run() {
        byte[] buffer = new byte[256];  // buffer store for the stream
        int bytes; // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs
        while (true) {
            try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer);        // Get number of bytes and message in "buffer"
                callback.onMessageReceived(bytes,buffer);
            } catch (IOException e) {
                break;
            }
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void write(String message) {
        if(btSocket.isConnected()) {
            Log.d(TAG, "...Data to send: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.d(TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }
    }

    /* Call this from the main activity to send data to the remote device */
    public void writeByte(byte mByte) {
        if(btSocket.isConnected()) {
            Log.d(TAG, "...Data to send: " + new String(new byte[]{mByte}) + "...");
            try {
                mmOutStream.write(mByte);
            } catch (IOException e) {
                Log.d(TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }
    }
    public void outFlush() {
        if (mmOutStream != null) {
            try {
                mmOutStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface BTCallback {
        void onMessageReceived(int bytes,byte[] buffer);
    }
}