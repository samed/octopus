package sumomouse.io.octopus;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;

public class ColorChooserAct extends AppCompatActivity implements
        ColorPicker.OnColorChangedListener {

    private static final String TAG = "ColorChooserActivity";

    String device;
    TextView tvReceived;

    private ColorPicker picker;

    private ConnectedThread mConnectedThread = ConnectedThread.mConnectedThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_chooser);

        if(getIntent().getExtras() != null) {
            device = getIntent().getStringExtra("device");
        }

        bindLayout();
        setupColorPicker();

    }

    @Override
    public void onColorChanged(int color) {
        buildSendColor(color);
    }

    @SuppressLint("SetTextI18n")
    public void buildSendColor(int color) {
        String hexColor = String.format("%06X", (0xFFFFFF & color));

        String subR = hexColor.substring(0,2);
        String subG = hexColor.substring(2,4);
        String subB = hexColor.substring(4,6);


        int r = Integer.parseInt(subR, 16);
        int g = Integer.parseInt(subG, 16);
        int b = Integer.parseInt(subB, 16);


        byte rR = toByte(r);
        byte rG = toByte(g);
        byte rB = toByte(b);

        String msg = BluetoothCommands.START_CHAR + BluetoothCommands.RGB;// + new String(new byte[]{rR,rG,rB});
        if(mConnectedThread != null) {
            mConnectedThread.write(msg);
            mConnectedThread.writeByte(rR);
            mConnectedThread.writeByte(rG);
            mConnectedThread.writeByte(rB);
            mConnectedThread.write("\r");
            picker.setOldCenterColor(color);
            tvReceived.setText("Color: #"+hexColor+"\n Data: " + msg);
        }
    }

    byte toByte(int i)
    {
        byte[] result = new byte[4];

        result[0] = (byte) (i >> 24);
        result[1] = (byte) (i >> 16);
        result[2] = (byte) (i >> 8);
        result[3] = (byte) (i /*>> 0*/);

        return result[3];
    }

/*  try {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }*/




    private void setupColorPicker() {
        picker = (ColorPicker) findViewById(R.id.picker);
        picker.addOpacityBar((OpacityBar) findViewById(R.id.opacitybar));
        picker.addSaturationBar((SaturationBar) findViewById(R.id.saturationbar));
        picker.addValueBar((ValueBar) findViewById(R.id.valuebar));
        picker.setOnColorChangedListener(this);
        picker.setShowOldCenterColor(true);
    }

    private void bindLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildSendColor(picker.getColor());
                    picker.setOldCenterColor(picker.getColor());

                }
            });
        }

        tvReceived = (TextView) findViewById(R.id.tvReceived);
    }

}
