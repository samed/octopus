package sumomouse.io.octopus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LauncherActivity extends AppCompatActivity implements
View.OnClickListener{

    private static final String TAG = "LauncherActivity";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_ENABLE_BT = 3;

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private ConnectedThread mConnectedThread;
    private StringBuilder sb = new StringBuilder();

    Handler h;

    private final int RECEIVE_MESSAGE = 1;        // Status  for Handler

    @Bind(R.id.bt_light)
    Button lightButton;

    @Bind(R.id.bt_joystick)
    Button joystickButton;

    @Bind(R.id.bt_pid)
    Button pidButton;

    @Bind(R.id.bt_stop)
    Button stopButton;

    @Bind(R.id.bt_start)
    Button startButton;

    @Bind(R.id.bt_send)
    Button sendButton;

    @Bind(R.id.et_send)
    EditText editTextSend;

    @Bind(R.id.mainListView)
    ListView mainListView;

    private String device;
    private String address;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private List<String> terminalList  = new ArrayList<>();

    private  ArrayAdapter adapter;
    private ConnectedThread.BTCallback callback = new ConnectedThread.BTCallback() {
        @Override
        public void onMessageReceived(int bytes, byte[] buffer) {
            h.obtainMessage(RECEIVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Send to message queue Handler
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        ButterKnife.bind(this);

        lightButton.setOnClickListener(this);
        joystickButton.setOnClickListener(this);
        pidButton.setOnClickListener(this);
        startButton.setOnClickListener(this);
        stopButton.setOnClickListener(this);
        sendButton.setOnClickListener(this);


        adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, terminalList);

        // Assign adapter to ListView
        mainListView.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        new MaterialDialog.Builder(this)
                .title("Choose Robot")
                .items(new String[]{"RNBT-99AA","RNBT-9A17"})
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        device = text.toString();
                        setupBluetoothHandler();
                        checkBTState();
                        if(btSocket==null) {
                            connectToBTDevice();
                        } else if(!btSocket.isConnected()) {
                            connectToBTDevice();
                        }
                    }
                })
                .cancelable(false)
                .autoDismiss(true)
                .show();


    }


    @Override
    public void onResume() {
        super.onResume();
        if(mConnectedThread != null) {
            mConnectedThread.setCallback(callback);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(TAG, "...In onPause()...");


        if(mConnectedThread != null) {
            mConnectedThread.outFlush();
        }

    }

    @Override
    public void onClick(View view) {

        Intent i;

        switch(view.getId()) {
            case R.id.bt_light:
                i = new Intent(this, ColorChooserAct.class);
                startActivity(i);
                break;
            case R.id.bt_joystick:
                i = new Intent(this, JoystickActivity.class);
                startActivity(i);
                break;
            case R.id.bt_pid:
                i = new Intent(this, PIDActivity.class);
                startActivity(i);
                break;
            case R.id.bt_start:
                String msg = BluetoothCommands.START_CHAR + BluetoothCommands.START;
                if( mConnectedThread != null) {
                    mConnectedThread.write(msg);
                    mConnectedThread.write("\r");
                }
                break;
            case R.id.bt_stop:
                String msg1 = BluetoothCommands.START_CHAR + BluetoothCommands.STOP;
                if( mConnectedThread != null) {
                    mConnectedThread.write(msg1);
                    mConnectedThread.write("\r");
                }
                break;
            case R.id.bt_send:
                String msg2 = editTextSend.getText().toString();
                if( mConnectedThread != null) {
                    mConnectedThread.write(msg2);
                }
                terminalList.add("Sent: " + msg2);
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    Log.i(TAG, "Device Address from Activity: " + address);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Intent serverIntent;

        if (id == R.id.action_connect) {
            // Launch the DeviceListActivity to see devices and do scan
            serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkBTState() {
        // Check for Bluetooth support and then check to make sure it is turned on
        // Emulator doesn't support Bluetooth and will return null
        if(btAdapter==null) {
            errorExit("Fatal Error", "Bluetooth not support");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d(TAG, "...Bluetooth ON...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        if(Build.VERSION.SDK_INT >= 10){
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", UUID.class);
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                Log.e(TAG, "Could not create Insecure RFComm Connection",e);
            }
        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    private void connectToBTDevice() {

        if(address != null) {
            BluetoothDevice device = btAdapter.getRemoteDevice(address);

            final String deviceName = device.getName();
            // Two things are needed to make a connection:
            //   A MAC address, which we got above.
            //   A Service ID or UUID.  In this case we are using the
            //     UUID for SPP.

            try {
                btSocket = createBluetoothSocket(device);
            } catch (IOException e) {
                errorExit("Fatal Error", "Socket create failed: " + e.getMessage() + ".");
            }

            // Discovery is resource intensive.  Make sure it isn't going on
            // when you attempt to connect and pass your message.
            btAdapter.cancelDiscovery();

            Thread thread = new Thread() {
                @Override
                public void run() {
                    // Establish the connection.  This will block until it connects.
                    Looper.prepare();
                    toast("Connecting to " + deviceName);
                    Log.d(TAG, "...Connecting...");
                    try {
                        btSocket.connect();
                        Log.d(TAG, "....Connection ok...");
                        toast("Connected to " + deviceName);
                        // Create a data stream so we can talk to server.
                        Log.d(TAG, "...Create Socket...");

                        mConnectedThread = new ConnectedThread(btSocket, callback);
                        mConnectedThread.start();
                    } catch (IOException e) {
                        toast("Couldn't connect to " + deviceName);
                        toast(e.getMessage());
                        Log.i(TAG,"Could not connect: " + e.getMessage());
                        try {
                            btSocket.close();
                            toast("Socket closed");
                        } catch (IOException e2) {
                            errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
                        }
                    }
                    Looper.loop();
                }
            };
            if(!btSocket.isConnected()) {
                thread.start();
            }
        }
    }

    private void setupBluetoothHandler() {

        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter

        // Set up a pointer to the remote node using it's address.
        Set<BluetoothDevice> devices = btAdapter.getBondedDevices();
        for(BluetoothDevice d : devices) {
            if(d.getName().equals(device)) {
                address = d.getAddress();
                Log.i(TAG,"Address: " + address);
            }
        }

        h = new Handler(new Handler.Callback() {
            @SuppressLint("SetTextI18n")
            @Override
            public boolean handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECEIVE_MESSAGE:                                                   // if receive massage
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1);                 // create string from bytes array
                        sb.append(strIncom);                                                // append string
                        int endOfLineIndex = sb.indexOf("\r\n");                            // determine the end-of-line
                        if (endOfLineIndex > 0) {                                            // if end-of-line,
                            String sbPrint = sb.substring(0, endOfLineIndex);               // extract string
                            sb.delete(0, sb.length());                                      // and clear
                            Log.i(TAG,"Received: " + sbPrint);
                            terminalList.add("Received: " + sbPrint);
                            adapter.notifyDataSetChanged();
                        }
                        break;
                }
                return true;
            }
        });
    }


    private void toast(String text) {
        Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
    }


    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }
}
