package sumomouse.io.octopus;

/**
 * Created by xSor.cr on 4/5/2016.
 */
public class BluetoothCommands {
    public static final char RGB = 'L';
    public static final char MOTORS = 'M';
    public static final char E_STOP = 'S';
    public static final char STOP = 's';
    public static final String START_CHAR = "$";
    public static final char START = 'X';
    public static final char PID = 'P';

}
