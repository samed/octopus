package sumomouse.io.octopus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

public class JoystickActivity extends AppCompatActivity {

    private static final String TAG = "JoystickActivity";

    private static final double DEG_2_RAD = Math.PI/180;


    private ConnectedThread mConnectedThread = ConnectedThread.mConnectedThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);

        setupJoystick();
    }


    private void setupJoystick() {
        Joystick joystick = (Joystick) findViewById(R.id.joystick);
        if (joystick != null) {
            joystick.setJoystickListener(new JoystickListener() {
                @Override
                public void onDown() {
                    // ..
                }

                @Override
                public void onDrag(float degrees, float offset) {


                    Log.i(TAG,"Degrees: " + degrees);
                    Log.i(TAG,"Offset: " + offset);

                    double x = 25*offset*Math.cos(degrees*DEG_2_RAD);
                    double y = 75*offset*Math.sin(degrees*DEG_2_RAD);

                    long lSpeed = Math.round(y-x);
                    long rSpeed = Math.round(y+x);

                    Log.i(TAG,"lSpeed: " + lSpeed);
                    Log.i(TAG,"rSpeed: " + rSpeed);

                    byte bL=(byte)rSpeed;
                    byte bR =(byte)lSpeed;

                    System.out.println("the value of bL/bR is " + (int)bL +"/ " + (int)bR);

                    String msg = BluetoothCommands.START_CHAR + BluetoothCommands.MOTORS;
                    if( mConnectedThread != null) {
                        mConnectedThread.write(msg);
                        mConnectedThread.writeByte(bL);
                        mConnectedThread.writeByte(bR);
                        mConnectedThread.write("\r");
                    }


                }

                @Override
                public void onUp() {
                    if(mConnectedThread != null) {
                        mConnectedThread.write(BluetoothCommands.START_CHAR + BluetoothCommands.STOP + "\r");
                    }
                }
            });
        }
    }

}
